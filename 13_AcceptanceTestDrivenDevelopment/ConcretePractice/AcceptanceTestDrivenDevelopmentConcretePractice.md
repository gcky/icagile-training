# Acceptance Test Driven Development Concrete Practice

Work in pairs.

This activity is only provided in Java.  All source code needed to complete the activity is provided.

The instructions can be found in the Acceptance Driven Development Code Files folder.

*Whole Activity Time Box:* **20 minutes**
